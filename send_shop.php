<meta http-equiv="refresh" content="0; url=http://www.dayclocks.nl/index.html#/thanks-shop" />
<?php
    $from = "info@dayclocks.nl";
    setlocale(LC_MONETARY, 'nl_NL');

    $datum = date('Y-m-d H:i');

    $type = $_REQUEST["type"];
    $email = $_POST["email"];
    $artikel = $_POST["artikel"];
    $prijs = $_POST["prijs"];
    $naam = $_POST["naam"];
    $adres = $_POST["adres"];
    $postcode = $_POST["postcode"];
    $plaats = $_POST["plaats"];
    $land = $_POST["land"];
    $telefoon = $_POST["telefoon"];
    $kleur = $_POST["kleur"];
    $aantal = $_POST["aantal"];
    $abonnement = $_POST["abonnement"];
    $opmerking = $_POST["opmerking"];
    $ordernummer = $_POST["ordernummer"];
    $artikelnummer = $_POST["artikelnummer"];
    $uitvoering = $_POST["uitvoering"];
    $naam = $_POST["naam"];
    $adres = $_POST["adres"];
    $postcode = $_POST["postcode"];
    $plaats = $_POST["plaats"];
    $land = $_POST["land"];
    $telefoon = $_POST["telefoon"];
    $naamklant = $_POST["naam-klant"];
    $adresklant = $_POST["adres-klant"];
    $postcodeklant = $_POST["postcode-klant"];
    $plaatsklant = $_POST["plaats-klant"];
    $landklant = $_POST["land-klant"];
    $telefoonklant = $_POST["telefoon-klant"];
    $opmerkingklant = $_POST["opmerking-klant"];

    $totaalPrijs = intval($aantal) * tofloat($prijs);

    $messageCSV = $email.",".
        $artikel.",".
        $prijs.",".
        $naam .",".
        $adres.",".
        $postcode.",".
        $plaats.",".
        $land.",".
        $telefoon.",".
        $kleur.",".
        $aantal.",".
        $type.",".
        $opmerking.",";
        $totaalPrijs = intval($aantal) * tofloat($prijs);

    $messageJSON =  '
                {  datum : "'.$datum.'"
                 , email : "'.$email.'"
                 , artikel : "'.$artikel.'"
                 , aantal : "'.$aantal.'"
                 , prijs : "'.$prijs.'"
                 , naam : "'.$naam.'"
                 , adres : "'.$adres.'"
                 , postcode : "'.$postcode.'"
                 , plaats : "'.$plaats.'"
                 , land : "'.$land.'"
                 , telefoon : "'.$telefoon.'"
                 , kleur : "'.$kleur.'"
                 , aantal : "'.$aantal.'"
                 , type : "'.$type.'"
                 , opmerking : "'.$opmerking.'"
                } ';

    $messageShopJSON =  '
                    {  datum : "'.$datum.'"
                     , email : "'.$email.'"
                     , artikel : "'.$artikel.'"
                     , aantal : "'.$aantal.'"
                     , prijs : "'.$prijs.'"
                     , naam : "'.$naam.'"
                     , adres : "'.$adres.'"
                     , postcode : "'.$postcode.'"
                     , plaats : "'.$plaats.'"
                     , land : "'.$land.'"
                     , telefoon : "'.$telefoon.'"
                     , kleur : "'.$kleur.'"
                     , aantal : "'.$aantal.'"
                     , type : "'.$type.'"
                     , opmerking : "'.$opmerking.'"
                     , ordernummer : "'.$ordernummer.'"
                     , artikelnummer : "'.$artikelnummer.'"
                     , uitvoering : "'.$uitvoering.'"
                     , naamklant : "'.$naamklant.'"
                     , adresklant : "'.$adresklant.'"
                     , postcodeklant : "'.$postcodeklant.'"
                     , plaatsklant : "'.$plaatsklant.'"
                     , landklant : "'.$landklant.'"
                     , telefoonklant : "'.$telefoonklant.'"
                     , opmerkingklant : "'.$opmerkingklant.'"
                    } ';


    $message = "<h2>Bestelling</h2>".
               "<table>".
               "<tr><td>Artikel</td><td>$artikel</td></tr>".
               "<tr><td>Aantal</td><td>$artikel</td></tr>".
               "<tr><td>Kleur</td><td>$kleur</td></tr>".
               "<tr><td>Prijs per stuk</td><td>$prijs</td></tr>".
               "<tr><td>Abonnement</td><td>$abonnement</td></tr>".
               "<tr><td>Totaal prijs</td><td>$totaalPrijs</td></tr>".
               "<tr><td>Naam</td><td>$naam</td></tr>".
               "<tr><td>Adres</td><td>$adres</td></tr>".
               "<tr><td>Postcode</td><td>$postcode</td></tr>".
               "<tr><td>Plaats</td><td>$plaats</td></tr>".
               "<tr><td>Land</td><td>$land</td></tr>".
               "<tr><td>Telefoon</td><td>$telefoon</td></tr>".
               "<tr><td>Email</td><td>$email</td></tr>".
               "<tr><td>Opmerking</td><td>$opmerking</td></tr>".
               "</table>".
               "<p></p>".
               "<p>Per email ontvangt U nog een betalingsverzoek.</p>".
               "<p></p>".
               "<p>Met vriendelijke groeten,</p> <p><i>Marcel van Lier</i></p>";


    $messageShop = "<h2>Bestelling</h2>".
               "<table>".
               "<tr><td><h2>Winkelgegevens</h2</td><td></td></tr>".
               "<tr><td>Artikel</td><td>$artikel</td></tr>".
               "<tr><td>Aantal</td><td>$artikel</td></tr>".
               "<tr><td>Kleur</td><td>$kleur</td></tr>".
               "<tr><td>Prijs per stuk</td><td>$prijs</td></tr>".
               "<tr><td>Abonnement</td><td>$abonnement</td></tr>".
               "<tr><td>Totaal prijs</td><td>$totaalPrijs</td></tr>".
               "<tr><td>Naam</td><td>$naam</td></tr>".
               "<tr><td>Adres</td><td>$adres</td></tr>".
               "<tr><td>Postcode</td><td>$postcode</td></tr>".
               "<tr><td>Plaats</td><td>$plaats</td></tr>".
               "<tr><td>Land</td><td>$land</td></tr>".
               "<tr><td>Telefoon</td><td>$telefoon</td></tr>".
               "<tr><td>Email</td><td>$email</td></tr>".
               "<tr><td>Opmerking</td><td>$opmerking</td></tr>".
               "<tr><td><h2>Klantgegevens</h2</td><td></td></tr>".
               "<tr><td>Naam</td><td>$naam</td></tr>".
                "<tr><td>Adres</td><td>$adres</td></tr>".
                "<tr><td>Postcode</td><td>$postcode</td></tr>".
                "<tr><td>Plaats</td><td>$plaats</td></tr>".
                "<tr><td>Land</td><td>$land</td></tr>".
                "<tr><td>Telefoon</td><td>$telefoon</td></tr>".
                "<tr><td>Email</td><td>$email</td></tr>".
                "<tr><td>Opmerking</td><td>$opmerking</td></tr>".
               "</table>".
               "<p></p>".
               "<p>Per email ontvangt U nog een betalingsverzoek.</p>".
               "<p></p>".
               "<p>Met vriendelijke groeten,</p> <p><i>Marcel van Lier</i></p>";


    // send mail
               // Always set content-type when sending HTML email
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: info@dayclocks.nl' . "\r\n";
   // $headers .= 'Bcc: henk.jurriens@gmail.com' . "\r\n";


    if ($ordernummer == "") {
      mail("$email, info@dayclocks.nl","Bestelling Dayclocks.nl", $message,$headers);
      mail("$email, henk.jurriens@gmail.com","Bestelling JSON Dayclocks.nl", $messageJSON,$headers);
    } else {
      mail("$email, info@dayclocks.nl","Bestelling Dayclocks.nl", $messageShop,$headers);
      mail("$email, henk.jurriens@gmail.com","Bestelling JSON Dayclocks.nl", $messageShopJSON,$headers);
    }


    function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
}

?>
